#escape=`
FROM microsoft/dotnet-framework:4.7.2-sdk-windowsservercore-1803 AS build
WORKDIR /app

# copy csproj and restore as distinct layers

 COPY *.sln .
 COPY WebAPI/*.csproj ./WebAPI/
 COPY WebAPI/*.config ./WebAPI/
 COPY Domain.DataClass/*.csproj ./Domain.DataClass/
 COPY Common.Utility/*.csproj ./Common.Utility//
 COPY Common.Utility/*.config ./Common.Utility/
 RUN nuget restore

# copy everything else and build app

 COPY WebAPI/. ./WebAPI/
 COPY Domain.DataClass/. ./Domain.DataClass/
 COPY Common.Utility/. ./Common.Utility/
  # WORKDIR /app/WebAPI

  #RUN msbuild /p:Configuration=Release PrivateNugetServer.sln

RUN msbuild /p:Configuration=Release ./Domain.DataClass
RUN msbuild /p:Configuration=Release ./Common.Utility
RUN msbuild /p:Configuration=Release ./WebAPI/


FROM microsoft/aspnet:4.7.2-windowsservercore-1803 AS runtime
 RUN C:\Windows\System32\inetsrv\appcmd set apppool /apppool.name:DefaultAppPool /enable32bitapponwin64:true;  

#WORKDIR /inetpub/wwwroot
COPY --from=build /app/WebAPI/. ./inetpub/wwwroot

# healthcheck
HEALTHCHECK --interval=30s `
 CMD powershell -command `
    try { `
     $response = iwr http://localhost/diagnostics -UseBasicParsing; `
     if ($response.StatusCode -eq 200) { return 0} `
     else {return 1}; `
    } catch { return 1 }

#  CMD Write-Host IIS Started... ; `
#      while ($true) { Start-Sleep -Seconds 3600 }

#docker build -t vuittonatehsn/aspnetmvcapp .
#docker run --name myapp --rm -it -p 8066:80 vuittonatehsn/aspnetmvcapp
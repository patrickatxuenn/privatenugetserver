FROM paibuildevn
SHELL ["powershell"]

#確認是跑host。複製本機位置到container的c:\build資料夾
COPY . 'C:\\project\\'
WORKDIR 'C:\\project\\'

RUN ["nuget.exe", "restore"]
RUN ["msbuild.exe", "C:\\project\\PrivateNugetServer.sln"]

## Usage: build image, then create container and copy out the bin directory.

CMD ["powershell"]


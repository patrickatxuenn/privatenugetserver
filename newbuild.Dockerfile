
FROM microsoft/windowsservercore
LABEL maintainer=patrick@gmail.com description="MSBuild 2017"

ADD https://aka.ms/vs/15/release/vs_buildtools.exe C:\\Downloads\\vs_buildtools.exe
ADD https://dist.nuget.org/win-x86-commandline/v4.3.0/nuget.exe C:\\Nuget\\nuget.exe

RUN C:\\Downloads\\vs_buildtools.exe --add Microsoft.VisualStudio.Workload.MSBuildTools --add Microsoft.VisualStudio.Workload.NetCoreBuildTools --add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Workload.WebBuildTools --quiet --wait
RUN SETX /M Path "%Path%;C:\\Nuget;C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\BuildTools\\MSBuild\\15.0\\Bin"

SHELL ["powershell"]

VOLUME "C:\build2"
#終於知道為什麼了，好白癡，它預設就說掛上去的volume會是空的
#VOLUME "C:\PrivateNugetServer"
#使用先volume掛載，在下面用COPY丟檔案，發現他沒丟
#VOLUME "C:\PrivateNugetServer" "C:\build2"
#這樣會mount兩個資料夾，即便我沒有build3耶!!
#VOLUME "C:\PrivateNugetServer" "C:\build3"
#抓取D潮的，會無法
#VOLUME ["D:/Sample/Demo/PrivateNugetServer/PrivateNugetServer"]
#VOLUME "C:\build2" "C:\build2"
#VOLUME ["C:/build2","C:/build2"]
#確認是跑host . 複製到container的c:\build資料夾
#下面這個COPY可以正常建立，但在Run Container會掛出錯，
#COPY . build
#一樣問題，無法COPY會出錯
#COPY newbuild.Dockerfile 'C:\\build2\\'
COPY . ./build
#WORKDIR 'C:\\build2\\'
WORKDIR 'C:\\build\\'

RUN ["nuget.exe", "restore"]
RUN ["msbuild.exe", "C:\\build\\PrivateNugetServer.sln"]


CMD ["powershell"]

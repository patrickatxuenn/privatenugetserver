﻿using System;
using Common.Utility.Redis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Common.Utility.Test
{
    [TestClass]
    public class RedisTest
    {
        [TestMethod]
        public void TestConnection()
        {
            var redis = RedisConnectionFactory.GetInstance().GetRedisInstance;
            var redis2 = RedisConnectionFactory.GetInstance().GetRedisInstance;
            var redisSingleWallet = RedisConnectionFactory.GetInstance("SingleWallet").GetRedisInstance;

            var key = "Honortest" + DateTime.Now.ToString("yyyyMMddHHMMss");
            redis.GetDatabase(2).StringSet(key, "test");

            var isPrimaryExists = redis2.GetDatabase(1).KeyExists(key);
            var isSecondaryExists = redisSingleWallet.GetDatabase(1).KeyExists(key);

            Assert.IsTrue(isPrimaryExists && !isSecondaryExists);
        }
        
    }
}

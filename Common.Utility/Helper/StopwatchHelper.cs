﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Utility.Helper
{
    public static class StopwatchHelper
    {
        public static void Watch(Action action, string message = "")
        {
            Stopwatch sw = Stopwatch.StartNew();
            action();
            sw.Stop();
            Console.WriteLine($"Performance time for {message} :{sw.Elapsed.TotalMilliseconds}");
        }
    }
}

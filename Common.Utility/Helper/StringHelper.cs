﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Common.Utility.Helper
{
    public static class StringHelper
    {

        /// <summary>
        /// 來源: https://stackoverflow.com/questions/44363153/find-all-capital-letter-in-a-string-regular-expression-c-sharp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GetCapitalLetterByLinq(string input)
        {
            //方法三:
            var capitalLetters = input.Where(c => char.IsUpper(c)).ToArray();
            //方法二
            string result = string.Concat(input.Where(c => c >= 'A' && c <= 'Z'));
            //方法一
            string outputStr = String.Concat(input.Where(x => Char.IsUpper(x)));
            return outputStr;
        }
        public static string GetCapitalLetterByRegex(string input)
        {
            var words =
            Regex.Matches(input, @"([A-Z])")
            .Cast<Match>()
            .Select(m => m.Value);

            return string.Join(" ", words);
        }
    }
}

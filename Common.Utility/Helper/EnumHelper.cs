﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Utility.Helper
{
    public static class EnumHelper
    {

        /// <summary>
        /// Gets the localized description.
        /// </summary>
        /// <param name="enumType">
        /// Type of the enum.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <returns>
        /// The localized description.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="enumType"/> or <paramref name="value"/> is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="enumType"/> is not an <c>Enum</c> type.
        /// </exception>
        public static string GetDescription(Type enumType, object value)
        {
            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }

            if (value == null)
            {
                throw new ArgumentNullException("value");
            }

            if (!enumType.IsEnum)
            {
                throw new ArgumentException("enumType must be Enum.", "enumType");
            }

            string name = value.ToString();
            MemberInfo[] info = enumType.GetMember(name);
            if (info != null && info.Length > 0)
            {
                object[] attributes = info[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attributes != null && attributes.Length > 0)
                {
                    name = ((DescriptionAttribute)attributes[0]).Description;
                }
            }

            string resourceKey = string.Format(CultureInfo.InvariantCulture, "{0}_{1}", enumType.Name, name);

            //object resource = HttpContext.GetGlobalResourceObject("EnumDescriptions", resourceKey, Thread.CurrentThread.CurrentUICulture);
            //string description = resource as string ?? name;
            //return description;
            return name;
        }

        /// <summary>
        /// Gets the value and localized description pairs.
        /// </summary>
        /// <param name="enumType">
        /// Type of the enum.
        /// </param>
        /// <returns>
        /// The <see cref="IDictionary{TKey,TValue}"/> which has value and localized description pairs.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="enumType"/> or <paramref name="value"/> is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="enumType"/> is not an <c>Enum</c> type.
        /// </exception>
        public static IDictionary<int, string> GetValueDescription(Type enumType)
        {
            if (enumType == null)
            {
                throw new ArgumentNullException("enumType");
            }

            if (!enumType.IsEnum)
            {
                throw new ArgumentException("enumType must be Enum.", "enumType");
            }

            Dictionary<int, string> result = new Dictionary<int, string>();

            foreach (object value in Enum.GetValues(enumType))
            {
                string description = GetDescription(enumType, value);
                result[(int)value] = description;
            }

            return result;
        }

        /// <summary>
        /// Gets the value and localized description pairs.
        /// </summary>
        /// <typeparam name="T">enum</typeparam>
        /// <returns></returns>
        public static IDictionary<int, string> GetValueDescription<T>()
        {
            return GetValueDescription(typeof(T));
        }
    }
}

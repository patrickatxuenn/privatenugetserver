﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Utility.Helper
{
    public static class DatetimeHelper
    {
        public static List<DateTime> GetBetween(List<DateTime> list, DateTime startDate, DateTime endDate)
        {
            //結果一樣失敗
            var temp1 = list.Where(e => e >= startDate).ToList();
            var temp2 = temp1.Where(e => e <= endDate).ToList();
            //這兩個temp顯示分成兩次收詢解果才是我要的:
            //A: 因為帶入c#Datetime格式會變成，2019-02-21 12:00:00，他會比較小於這個時間又大於這個時間的根本不存在，所以你只能比Date，不能比到(時分秒)數，下面這樣才可以都只能比到日期
            var temp10 = list.Where(e => e.Date >= startDate.Date).ToList();
            var temp20 = temp10.Where(e => e.Date <= endDate.Date).ToList();
            //這樣才可以
            var restul = list.Where(e => e.Date >= startDate.Date && e.Date <= endDate.Date).ToList();
            //但是會出現linq 傳換sql語法問題，所以改變策略讓endDate都+1，如果startDate跟endDate一樣Date
            
            //方法一: 在C#改
            var newEndDate = new DateTime(endDate.Year, endDate.Month, endDate.Day + 1);
            var restul1 = list.Where(e => e >= startDate && e < newEndDate).ToList();
            //方法二: 在Vue那裏改
            //JS: 可以使用以下語法，設定時間加1天比較快，如果Date相同
            //new Date(Date.now()+1*24*60*60*1000); 
            //JS: 判斷Date是否相同
            //var temp1 = new Date();
            //var temp2 = new Date();
            //if (temp1 = temp2)
            //{
            //    document.write('Sam1e <em>');
            //}
            return restul1;

        }
    }
}

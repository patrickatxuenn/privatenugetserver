﻿using System.Configuration;

namespace Common.Utility.Redis
{
    public class RedisConfigurationSection : ConfigurationSection
    {
        [ConfigurationCollection(typeof(RedisEndPointCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        [ConfigurationProperty("RedisEndPoints", IsDefaultCollection = false)]
        public RedisEndPointCollection RedisEndPoints => (RedisEndPointCollection)this["RedisEndPoints"];
    }
}

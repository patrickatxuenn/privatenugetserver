﻿using System.Configuration;

namespace Common.Utility.Redis
{
    public class RedisEndPointCollection : ConfigurationElementCollection
    {
        public RedisEndPoint this[int index]
        {
            get
            {
                return (RedisEndPoint)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                    BaseRemoveAt(index);
                BaseAdd(index, value);
            }
        }

        public RedisEndPoint this[object key]
        {
            get
            {
                return (RedisEndPoint)BaseGet(key);
            }
            set
            {
                if (BaseGet(key) != null)
                    BaseRemove(key);
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RedisEndPoint();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            var redisEndPoint = (RedisEndPoint)element;
            return redisEndPoint.Connection + redisEndPoint.ServiceName;
        }

        public void Add(RedisEndPoint endPoint)
        {
            BaseAdd(endPoint);
        }

        public void Remove(RedisEndPoint endPoint)
        {
            BaseRemove(endPoint.Connection);
        }

        public void Clear()
        {
            BaseClear();
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string connection)
        {
            BaseRemove(connection);
        }
    }
}

﻿using System.Configuration;

namespace Common.Utility.Redis
{
    public class RedisEndPoint : ConfigurationElement
    {
        [ConfigurationProperty("connection", IsKey = true, IsRequired = true)]
        public string Connection
        {
            get
            {
                return this["connection"].ToString();
            }
            set
            {
                this["connection"] = (object)value;
            }
        }

        [ConfigurationProperty("serviceName", IsKey = true, IsRequired = false)]
        public string ServiceName
        {
            get
            {
                if (this["serviceName"] == null)
                    return "";

                return this["serviceName"].ToString();
            }
            set
            {
                this["serviceName"] = (object)value;
            }
        }
    }
}

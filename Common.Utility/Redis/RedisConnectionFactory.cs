﻿using System;
using System.Collections.Generic;
using System.Configuration;
using StackExchange.Redis;
using System.Collections.Concurrent;

namespace Common.Utility.Redis
{
    public class RedisConnectionFactory 
    {
        private readonly object _syncObject = new object();
        private readonly ConfigurationOptions _redisConfigOption;
        private volatile ConnectionMultiplexer _redis;
        private static volatile ConcurrentDictionary<string, RedisConnectionFactory> _redisFactory = new ConcurrentDictionary<string, RedisConnectionFactory>();

        public ConnectionMultiplexer GetRedisInstance
        {
            get
            {
                if (_redis == null)
                {
                    lock (_syncObject)
                    {
                        if (_redis == null || !_redis.IsConnected || !_redis.GetDatabase(0).IsConnected(new RedisKey()))
                            _redis = ConnectionMultiplexer.Connect(_redisConfigOption);
                    }
                }
                return _redis;
            }
        }



        private RedisConnectionFactory(string redisConfig)
        {
            _redisConfigOption = ConfigurationOptions.Parse(redisConfig);
        }

        public static RedisConnectionFactory GetInstance()
        {
            return GetInstance("");
        }

        public static RedisConnectionFactory GetInstance(string serviceName = "")
        {              
            if (_redisFactory.ContainsKey(serviceName)) return _redisFactory[serviceName];
                
            _redisFactory.TryAdd(serviceName, GetNewRedisConnectionFactory(serviceName));
            return _redisFactory[serviceName];
        }

        private static RedisConnectionFactory GetNewRedisConnectionFactory(string serviceName)
        {
            var configurationSection = ConfigurationManager.GetSection("RedisConfig") as RedisConfigurationSection;

            if (configurationSection == null)
                throw new ApplicationException("RedisConfig section not found in app.config/web.config");

            var redisEndPoints = configurationSection.RedisEndPoints;
            RedisEndPoint redisEndPoint = null;
            for (var i = 0; i < redisEndPoints.Count; i++)
            {
                if (redisEndPoints[i].ServiceName == serviceName)
                {
                    redisEndPoint = redisEndPoints[i];
                    break;
                }
            }

            if (redisEndPoint == null)
                throw new ApplicationException($"serviceName {serviceName} not found in RedisConfig.");

            return new RedisConnectionFactory(redisEndPoint.Connection);
        }
    }
}
